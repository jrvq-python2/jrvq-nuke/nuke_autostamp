# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: auto_stamp_exec.py
Date: 2019.10.04
Revision: 2019.10.23
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


import getpass
import datetime

import nuke

# -------------------------------- DEFAULT VALUES -------------------------------- #

# KNOBS NAMING
GROUP_NAME = 'AUTO_STAMP_'
KNOB_BASE_NAME = 'autostamp_'

SEQ_INPUT_NAME = KNOB_BASE_NAME + 'seq_input'
SHOT_INPUT_NAME = KNOB_BASE_NAME + 'shot_input'
ARTIST_INPUT_NAME = KNOB_BASE_NAME + 'artist_input'
ARTIST_INPUT_NAME = KNOB_BASE_NAME + 'artist_input'
DEPARTMENT_INPUT_NAME = KNOB_BASE_NAME + 'department_input'
STATE_INPUT_NAME = KNOB_BASE_NAME + 'state_input'
COMMENT_INPUT_NAME = KNOB_BASE_NAME + 'comment_input'
DATE_INPUT_NAME = KNOB_BASE_NAME + 'date_input'
DATE_BUTTON_NAME = KNOB_BASE_NAME + 'date_script_btn'

TEXT_COLOR_NAME = KNOB_BASE_NAME + 'stamp_text_color'
BACKGROUND_COLOR_CHECKBOX = KNOB_BASE_NAME + 'use_stamp_background'
BACKGROUND_COLOR_NAME = KNOB_BASE_NAME + 'stamp_background_color'

# DEFAULT VALUES FOR KNOBS
DEPARTMENTS = ['Concept art', 'Layout', 'Animation', 'Set dressing', 'Characters FX',
               'Lighting', 'FX', 'Compositing', 'Color grading']
STATES = ['Blocking', 'WIP', 'Refine', 'To send to client', 'For director', 'CBB']

DEFAULT_SEQ = 'test'
DEFAULT_SHOT = 0
current_user = getpass.getuser()
default_date = datetime.datetime.now().strftime("%Y.%m.%d - %H:%M")


PARAMS_LIST = [{'name' : 'SEQ_INFO_TXT',
                'message' : 'SEQ: [string toupper [value parent.{}]]\n'
                            'SHOT: [format %04s [value parent.{}]]\n'
                            'FRAME: [format %06s [frame]]'
                            ''.format(SEQ_INPUT_NAME, SHOT_INPUT_NAME),
                'box.x' : 0.05, 'box.y' : 0, 'box.r' : 1, 'box.t' : 0.95,
                'alignment' : 'left', 'font' : 0.4},

                {'name' : 'DEPT_INFO',
                 'message' : '[value parent.{}]'.format(DEPARTMENT_INPUT_NAME),
                 'box.x' : 0.05, 'box.y' : 0, 'box.r' : 0.95, 'box.t' : 0.95,
                 'alignment' : 'center', 'font' : 0.5},

                {'name': 'STATE_INFO',
                 'message': '[value parent.{}] - [value parent.{}]'.format(STATE_INPUT_NAME, COMMENT_INPUT_NAME),
                 'box.x': 0.05, 'box.y': 0, 'box.r': 0.95, 'box.t': 0.89,
                 'alignment' : 'center', 'font': 0.35},

                {'name': 'DATE_INFO',
                 'message': '[value parent.{}]\n'
                            '[value parent.{}].[format %04s [value parent.{}]]'
                            ''.format(DATE_INPUT_NAME, SEQ_INPUT_NAME, SHOT_INPUT_NAME),
                 'box.x': 0.05, 'box.y': 0, 'box.r': 0.95, 'box.t': 0.95,
                 'alignment' : 'right', 'font': 0.4},

                {'name': 'ARTIST_INFO_TXT',
                 'message': 'Artist: [value parent.{}]'.format(ARTIST_INPUT_NAME),
                 'box.x': 0.05, 'box.y': 0, 'box.r': 0.95, 'box.t': 0.13,
                 'alignment' : 'left', 'font': 0.4},

                {'name': 'FILE_INFO',
                 'message': 'File: [lindex [split [value root.name] / ] end]',
                 'box.x': 0.05, 'box.y': 0, 'box.r': 0.95, 'box.t': 0.13,
                 'alignment' : 'right', 'font': 0.4}
               ]


# -------------------------------- GROUP CREATION -------------------------------- #

# GROUP
g_count = 1
while nuke.toNode(GROUP_NAME + str(g_count)):
    g_count += 1

g = nuke.nodes.Group(name=GROUP_NAME + str(g_count))

# INPUTS TAB
stamp_info_tab = nuke.Tab_Knob(KNOB_BASE_NAME + 'stamp_input_tab', 'Stamp input')
g.addKnob(stamp_info_tab)

shot_info_label = nuke.Text_Knob(KNOB_BASE_NAME + 'shot_info_label', '', '<font color=white size=5>Shot information')
g.addKnob(shot_info_label)

seq_name_input = nuke.String_Knob(SEQ_INPUT_NAME, 'Seq:', DEFAULT_SEQ)
g.addKnob(seq_name_input)

shot_num_input = nuke.Int_Knob(SHOT_INPUT_NAME, '     Shot: ', DEFAULT_SHOT)
shot_num_input.clearFlag(nuke.STARTLINE)
g.addKnob(shot_num_input)

artist_name_input = nuke.String_Knob(ARTIST_INPUT_NAME, 'Artist:', current_user)
g.addKnob(artist_name_input)

first_separator = nuke.Text_Knob(KNOB_BASE_NAME + 'first_separator', '', '')
g.addKnob(first_separator)

# ------- #

department_info_label = nuke.Text_Knob(KNOB_BASE_NAME + 'department_info_label', '',
                                       '<font color=white size=5>Department and state')
g.addKnob(department_info_label)

department_name_input = nuke.Enumeration_Knob(DEPARTMENT_INPUT_NAME, 'Dept:', DEPARTMENTS)
g.addKnob(department_name_input)

state_input = nuke.Enumeration_Knob(STATE_INPUT_NAME, '     State: ', STATES)
state_input.clearFlag(nuke.STARTLINE)
g.addKnob(state_input)

comment_input = nuke.String_Knob(COMMENT_INPUT_NAME, 'Comment:', 'Your comment here')
g.addKnob(comment_input)

second_separator = nuke.Text_Knob(KNOB_BASE_NAME + 'second_separator', '', '')
g.addKnob(second_separator)

# ------- #

date_info_label = nuke.Text_Knob(KNOB_BASE_NAME + 'date_info_label', '', '<font color=white size=5>Date')
g.addKnob(date_info_label)

date_input = nuke.String_Knob(DATE_INPUT_NAME, 'Date:', default_date)
g.addKnob(date_input)

date_button = nuke.PyScript_Knob(DATE_BUTTON_NAME, 'SET DATE')
set_date_script = 'import datetime' \
                  '\nd = datetime.datetime.now().strftime("%Y.%m.%d - %H:%M")' \
                  '\nnuke.thisNode()["{}"].setValue(d)'.format(DATE_INPUT_NAME)
date_button.setCommand(set_date_script)
g.addKnob(date_button)


# TEXT COLORS TAB
colors_tab = nuke.Tab_Knob(KNOB_BASE_NAME + 'text_colors_tab', 'Text colors')
g.addKnob(colors_tab)

stamp_color_label = nuke.Text_Knob(KNOB_BASE_NAME + 'Stamp colors', '', '<font color=white size=5>Text color')
g.addKnob(stamp_color_label)

stamp_text_color = nuke.Color_Knob(TEXT_COLOR_NAME, 'Stamp text color:')
stamp_text_color.setValue([1,1,1])
g.addKnob(stamp_text_color)

colors_separator = nuke.Text_Knob(KNOB_BASE_NAME + 'second_separator', '', '')
g.addKnob(colors_separator)

# ------- #

use_background_color_checkbox = nuke.Boolean_Knob(BACKGROUND_COLOR_CHECKBOX, 'Use background color?', True)
use_background_color_checkbox.setFlag(nuke.STARTLINE)
g.addKnob(use_background_color_checkbox)

stamp_background_color = nuke.Color_Knob(BACKGROUND_COLOR_NAME, 'Background color:')
g.addKnob(stamp_background_color)


g.begin()


# -------------------------------- INTERIOR NODES -------------------------------- #

# INPUT
g_input = nuke.nodes.Input()

# OUTPUT
g_output = nuke.nodes.Output()

# INTERIOR NODES
for i in range (len(PARAMS_LIST)):

    node_dict = PARAMS_LIST[i]

    n = nuke.nodes.Text2(name = node_dict['name'])

    # Dimensions setup
    n.knob('box').setExpression('parent.width * {}'.format(node_dict['box.x']), 0)
    n.knob('box').setExpression('parent.width * {}'.format(node_dict['box.y']), 2)
    n.knob('box').setExpression('parent.width * {}'.format(node_dict['box.r']), 2)
    n.knob('box').setExpression('parent.height * {}'.format(node_dict['box.t']), 3)

    # Font setup
    n.knob('xjustify').setValue(node_dict['alignment'])
    n.knob('global_font_scale').setExpression('parent.height/1080 * {}'.format(node_dict['font']))

    # Message setup
    n.knob('message').setValue(node_dict['message'])

    # Color setup
    n.knob('color').setSingleValue(False)
    n.knob('color').setExpression('parent.{}.r'.format(TEXT_COLOR_NAME), 0)
    n.knob('color').setExpression('parent.{}.g'.format(TEXT_COLOR_NAME), 1)
    n.knob('color').setExpression('parent.{}.b'.format(TEXT_COLOR_NAME), 2)

    n.knob('enable_background').setExpression('parent.{}'.format(BACKGROUND_COLOR_CHECKBOX))
    n.knob('background_color').setSingleValue(False)
    n.knob('background_color').setExpression('parent.{}.r'.format(BACKGROUND_COLOR_NAME), 0)
    n.knob('background_color').setExpression('parent.{}.g'.format(BACKGROUND_COLOR_NAME), 1)
    n.knob('background_color').setExpression('parent.{}.b'.format(BACKGROUND_COLOR_NAME), 2)


    if i == 0:
        n.setInput(0, g_input)
    else:
        n.setInput(0, nuke.toNode(PARAMS_LIST[i-1]['name']))
        if i == len(PARAMS_LIST) - 1:
            g_output.setInput(0, n)


# END GROUP
g.end()


# -------------------------------- FRAMING -------------------------------- #
if len(nuke.selectedNodes()) == 1:
    g.setInput(0, nuke.selectedNode())
for n in nuke.allNodes():
    n.setSelected(False)
g.setSelected(True)
stamp_info_tab.setFlag(0)
nuke.zoomToFitSelected()
g.showControlPanel()
